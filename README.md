# Gamepad support for Salet 2

Requires [Salet](https://gitlab.com/Oreolek/salet-module) 2+

Uses code from the [`html-gamepad`](https://github.com/ericlathrop/html5-gamepad/blob/master/LICENSE) library in accordance to MIT License.

Copyright 2017 Alexander Yakovlev.

`html-gamepad` code copyright 2016 Eric Lathrop, with contributions from Rex Soriano.
