$ = require('jquery')
retval = {}

# Select the previous choice
retval.selectUp = (event, button, padid) ->
  if button != "dpad up" and button != "left stick up"
    return
  if $(".options li").length == 0
    return selectUpLink(event, button, padid)
  $(".options li").removeClass("active")
  count = $(".options li").length
  window.selectedoption--
  if window.selectedoption <= 0
    window.selectedoption = count
  $(".options li:nth-child(#{window.selectedoption}").addClass("active")

# Select the next choice
retval.selectDown = (event, button, padid) ->
  if button != "dpad down" and button != "left stick down"
    return
  if $(".options li").length == 0
    return selectDownLink(event, button, padid)
  $(".options li").removeClass("active")
  window.selectedoption++
  count = $(".options li").length
  if window.selectedoption > count
    window.selectedoption = 1
  $(".options li:nth-child(#{window.selectedoption})").addClass("active")

# Select the next link inside room text
selectDownLink = (event, button, padid) ->
  tabid = $("#current-room .active").first().attr("tabindex")
  $("#current-room .active").removeClass("active")
  maxtab = 0
  for element in $("#current-room [tabindex]")
    if maxtab < $(element).attr("tabindex")
      maxtab = $(element).attr("tabindex")
  tabid = salet.view.increaseTabindex(tabid, maxtab)
  $("#current-room [tabindex='#{tabid}']").addClass("active")

# Select the previous link inside room text
selectUpLink = (event, button, padid) ->
  tabid = $("#current-room .active").attr("tabindex")
  $("#current-room .active").removeClass("active")
  tabid = salet.view.decreaseTabindex(tabid)
  $("#current-room [tabindex='#{tabid}']").addClass("active")

retval.decreaseTabindex = (tabid) ->
  tabid--
  if tabid < 0
    # tabindex can't be negative, choosing maximum tabindex
    maxtab = 0
    for element in $("#current-room [tabindex]")
      if maxtab < $(element).attr("tabindex")
        maxtab = $(element).attr("tabindex")
    tabid = maxtab
  if $("#current-room [tabindex='#{tabid}']").length == 0
    return salet.view.decreaseTabindex(tabid)
  return tabid

retval.increaseTabindex = (tabid, maxtab) ->
  if tabid < maxtab
    tabid++
  else
    tabid = 0
  if $("#current-room [tabindex='#{tabid}']").length == 0
    return salet.view.increaseTabindex(tabid)
  return tabid

selectOption = (event, button) ->
  if button != "a"
    return
  $(".options li.active").click()

# enter the options room
enterOptions = (event, button, padid) ->
  if button != "back" and button != "start"
    return
  if button == "start"
    return salet.goTo(salet.optionsRoom)
  if button == "back" and salet.currentRoom == salet.optionsRoom
    return salet.goBack()

# Joystick check, fires events
joystick = () ->
  if typeof navigator.getGamepads == "function"
    for pad in navigator.getGamepads()
      if pad?
        temp = new Gamepad(pad)
        if temp.pressed?
          if salet.view.gamepads[pad.id].pressed != temp.pressed
            $(document).trigger("press", temp.pressed, pad.id)
        if temp.map.axes?
          for axis, axisname in temp.map.axes
            oldval = salet.view.gamepads[pad.id].gamepad.axes[axis.index]
            newval = temp.gamepad.axes[axis.index]
            if Math.abs(newval) <= 0.2
              newval = 0
            if newval != oldval
              $(document).trigger("axis", axisname, newval, padid)
        salet.view.gamepads[pad.id] = temp

$(document).on("view_init", () ->
  # Connected gamepads (see Gamepad class)
  salet.view.gamepads = {}
  salet.view.gamepadmappings = mappings
  window.addEventListener("gamepadconnected", (e) ->
    salet.view.gamepads[e.gamepad.id] = new Gamepad(e.gamepad)
  )
  window.addEventListener("gamepaddisconnected", (e) ->
    salet.view.gamepads[e.gamepad.id] = undefined
  )
  setInterval(joystick, 50) # 50 = 1000 / 20 fps
)

window.selectedoption ?= 1
$(document).on("press", salet.view.selectUp)
$(document).on("press", salet.view.selectDown)
$(document).on("press", selectOption)
$(document).on("press", enterOptions)

module.exports = retval
